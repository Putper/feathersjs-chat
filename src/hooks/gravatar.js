// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

const crypto = require('crypto'); // required to create MD5 hash

const gravatarUrl = 'https://s.gravatar.com/avatar';  // Gravatar image service
const query = 's=60'; // The size query in pixels (px)

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async context => {
    const { email } = context.data; // user email
    const hash = crypto.createHash('md5').update(email).digest('hex');  // Gravatar uses MD5 hashes from an email address to get the image

    context.data.avatar = `${gravatarUrl}/${hash}?${query}`;

    return context;
  };
};