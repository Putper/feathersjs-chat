// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async context => {
    const { data } = context;
    
    // Throw error if no text in data
    if(!data.text) 
    {
      throw new Error('A message must have a text');
    }

    const user = context.params.user; // authenticated user
    const text = context.data.text  // message text
      .substring(0, 400); // Messages can't be longer than 400 characters

    // Override the original data (so people can't submit additional shit)
    context.data = {
      text,
      userId: user._id, // Set the user id
      createdAt: new Date().getTime() // Add the current date
    };

    return context;
  };
};
